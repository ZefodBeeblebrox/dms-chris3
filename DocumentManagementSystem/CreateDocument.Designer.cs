﻿namespace DocumentManagementSystem
{
    partial class CreateDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelCreateNewDocument = new System.Windows.Forms.Button();
            this.lblDocumentStatus = new System.Windows.Forms.Label();
            this.lblCreateDocumentRecord = new System.Windows.Forms.Label();
            this.txtDocumentTitle = new System.Windows.Forms.TextBox();
            this.lblRevisionNumber = new System.Windows.Forms.Label();
            this.lblDocumentTitle = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblAttach = new System.Windows.Forms.Label();
            this.lblAddDistributee = new System.Windows.Forms.Label();
            this.txtAddDistributee = new System.Windows.Forms.TextBox();
            this.btnAddDistributee = new System.Windows.Forms.Button();
            this.btnSubmitNewDocument = new System.Windows.Forms.Button();
            this.lblCurrentDistributees = new System.Windows.Forms.Label();
            this.lstbxDistributees = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ckbxActive = new System.Windows.Forms.CheckBox();
            this.ckbxDraft = new System.Windows.Forms.CheckBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtRevNum = new System.Windows.Forms.NumericUpDown();
            this.lblTargetURL = new System.Windows.Forms.Label();
            this.lblSourceURL = new System.Windows.Forms.Label();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblDestination = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRevNum)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelCreateNewDocument
            // 
            this.btnCancelCreateNewDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnCancelCreateNewDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelCreateNewDocument.Location = new System.Drawing.Point(1069, 583);
            this.btnCancelCreateNewDocument.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancelCreateNewDocument.Name = "btnCancelCreateNewDocument";
            this.btnCancelCreateNewDocument.Size = new System.Drawing.Size(149, 62);
            this.btnCancelCreateNewDocument.TabIndex = 39;
            this.btnCancelCreateNewDocument.Text = "Cancel";
            this.btnCancelCreateNewDocument.UseVisualStyleBackColor = false;
            this.btnCancelCreateNewDocument.Click += new System.EventHandler(this.btnCancelCreateNewDocument_Click);
            // 
            // lblDocumentStatus
            // 
            this.lblDocumentStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDocumentStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentStatus.Location = new System.Drawing.Point(623, 143);
            this.lblDocumentStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDocumentStatus.Name = "lblDocumentStatus";
            this.lblDocumentStatus.Size = new System.Drawing.Size(263, 34);
            this.lblDocumentStatus.TabIndex = 32;
            this.lblDocumentStatus.Text = "Document Status";
            // 
            // lblCreateDocumentRecord
            // 
            this.lblCreateDocumentRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreateDocumentRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateDocumentRecord.Location = new System.Drawing.Point(296, 15);
            this.lblCreateDocumentRecord.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCreateDocumentRecord.Name = "lblCreateDocumentRecord";
            this.lblCreateDocumentRecord.Size = new System.Drawing.Size(809, 76);
            this.lblCreateDocumentRecord.TabIndex = 30;
            this.lblCreateDocumentRecord.Text = "Create Document Record";
            // 
            // txtDocumentTitle
            // 
            this.txtDocumentTitle.Location = new System.Drawing.Point(309, 181);
            this.txtDocumentTitle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDocumentTitle.Name = "txtDocumentTitle";
            this.txtDocumentTitle.Size = new System.Drawing.Size(261, 22);
            this.txtDocumentTitle.TabIndex = 27;
            // 
            // lblRevisionNumber
            // 
            this.lblRevisionNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblRevisionNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRevisionNumber.Location = new System.Drawing.Point(309, 222);
            this.lblRevisionNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRevisionNumber.Name = "lblRevisionNumber";
            this.lblRevisionNumber.Size = new System.Drawing.Size(263, 34);
            this.lblRevisionNumber.TabIndex = 26;
            this.lblRevisionNumber.Text = "Revision Number";
            // 
            // lblDocumentTitle
            // 
            this.lblDocumentTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDocumentTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentTitle.Location = new System.Drawing.Point(309, 143);
            this.lblDocumentTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDocumentTitle.Name = "lblDocumentTitle";
            this.lblDocumentTitle.Size = new System.Drawing.Size(263, 34);
            this.lblDocumentTitle.TabIndex = 25;
            this.lblDocumentTitle.Text = "Document Title";
            // 
            // btnBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(309, 358);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(267, 63);
            this.btnBrowse.TabIndex = 24;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblAttach
            // 
            this.lblAttach.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblAttach.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttach.Location = new System.Drawing.Point(309, 320);
            this.lblAttach.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAttach.Name = "lblAttach";
            this.lblAttach.Size = new System.Drawing.Size(263, 34);
            this.lblAttach.TabIndex = 41;
            this.lblAttach.Text = "Attach Document";
            this.lblAttach.Click += new System.EventHandler(this.lblAttach_Click);
            // 
            // lblAddDistributee
            // 
            this.lblAddDistributee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblAddDistributee.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddDistributee.Location = new System.Drawing.Point(623, 250);
            this.lblAddDistributee.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddDistributee.Name = "lblAddDistributee";
            this.lblAddDistributee.Size = new System.Drawing.Size(263, 34);
            this.lblAddDistributee.TabIndex = 42;
            this.lblAddDistributee.Text = "Add Distributee";
            // 
            // txtAddDistributee
            // 
            this.txtAddDistributee.Location = new System.Drawing.Point(623, 288);
            this.txtAddDistributee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAddDistributee.Name = "txtAddDistributee";
            this.txtAddDistributee.Size = new System.Drawing.Size(261, 22);
            this.txtAddDistributee.TabIndex = 43;
            // 
            // btnAddDistributee
            // 
            this.btnAddDistributee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnAddDistributee.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDistributee.Location = new System.Drawing.Point(623, 320);
            this.btnAddDistributee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddDistributee.Name = "btnAddDistributee";
            this.btnAddDistributee.Size = new System.Drawing.Size(267, 63);
            this.btnAddDistributee.TabIndex = 44;
            this.btnAddDistributee.Text = "Add";
            this.btnAddDistributee.UseVisualStyleBackColor = false;
            // 
            // btnSubmitNewDocument
            // 
            this.btnSubmitNewDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnSubmitNewDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmitNewDocument.Location = new System.Drawing.Point(1069, 507);
            this.btnSubmitNewDocument.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSubmitNewDocument.Name = "btnSubmitNewDocument";
            this.btnSubmitNewDocument.Size = new System.Drawing.Size(149, 62);
            this.btnSubmitNewDocument.TabIndex = 45;
            this.btnSubmitNewDocument.Text = "Submit";
            this.btnSubmitNewDocument.UseVisualStyleBackColor = false;
            this.btnSubmitNewDocument.Click += new System.EventHandler(this.btnSubmitNewDocument_Click);
            // 
            // lblCurrentDistributees
            // 
            this.lblCurrentDistributees.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCurrentDistributees.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentDistributees.Location = new System.Drawing.Point(623, 400);
            this.lblCurrentDistributees.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCurrentDistributees.Name = "lblCurrentDistributees";
            this.lblCurrentDistributees.Size = new System.Drawing.Size(263, 34);
            this.lblCurrentDistributees.TabIndex = 46;
            this.lblCurrentDistributees.Text = "Add Distributee";
            // 
            // lstbxDistributees
            // 
            this.lstbxDistributees.FormattingEnabled = true;
            this.lstbxDistributees.ItemHeight = 16;
            this.lstbxDistributees.Items.AddRange(new object[] {
            " "});
            this.lstbxDistributees.Location = new System.Drawing.Point(623, 438);
            this.lstbxDistributees.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstbxDistributees.Name = "lstbxDistributees";
            this.lstbxDistributees.Size = new System.Drawing.Size(265, 132);
            this.lstbxDistributees.TabIndex = 48;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(272, 239);
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // ckbxActive
            // 
            this.ckbxActive.AutoSize = true;
            this.ckbxActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.ckbxActive.Location = new System.Drawing.Point(623, 201);
            this.ckbxActive.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ckbxActive.Name = "ckbxActive";
            this.ckbxActive.Size = new System.Drawing.Size(68, 21);
            this.ckbxActive.TabIndex = 49;
            this.ckbxActive.Text = "Active";
            this.ckbxActive.UseVisualStyleBackColor = false;
            // 
            // ckbxDraft
            // 
            this.ckbxDraft.AutoSize = true;
            this.ckbxDraft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.ckbxDraft.Location = new System.Drawing.Point(761, 201);
            this.ckbxDraft.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ckbxDraft.Name = "ckbxDraft";
            this.ckbxDraft.Size = new System.Drawing.Size(61, 21);
            this.ckbxDraft.TabIndex = 50;
            this.ckbxDraft.Text = "Draft";
            this.ckbxDraft.UseVisualStyleBackColor = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblDescription
            // 
            this.lblDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(941, 143);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(263, 34);
            this.lblDescription.TabIndex = 51;
            this.lblDescription.Text = "Description";
            this.lblDescription.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(941, 203);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(261, 132);
            this.txtDescription.TabIndex = 52;
            // 
            // txtRevNum
            // 
            this.txtRevNum.Location = new System.Drawing.Point(309, 260);
            this.txtRevNum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRevNum.Name = "txtRevNum";
            this.txtRevNum.Size = new System.Drawing.Size(263, 22);
            this.txtRevNum.TabIndex = 53;
            this.txtRevNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRevNum_KeyDown);
            this.txtRevNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRevNum_KeyPress);
            // 
            // lblTargetURL
            // 
            this.lblTargetURL.BackColor = System.Drawing.Color.White;
            this.lblTargetURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblTargetURL.Location = new System.Drawing.Point(312, 530);
            this.lblTargetURL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTargetURL.Name = "lblTargetURL";
            this.lblTargetURL.Size = new System.Drawing.Size(263, 34);
            this.lblTargetURL.TabIndex = 54;
            this.lblTargetURL.Click += new System.EventHandler(this.lblFileURL_Click);
            // 
            // lblSourceURL
            // 
            this.lblSourceURL.BackColor = System.Drawing.Color.White;
            this.lblSourceURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblSourceURL.Location = new System.Drawing.Point(312, 454);
            this.lblSourceURL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSourceURL.Name = "lblSourceURL";
            this.lblSourceURL.Size = new System.Drawing.Size(263, 34);
            this.lblSourceURL.TabIndex = 55;
            // 
            // lblSource
            // 
            this.lblSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold);
            this.lblSource.Location = new System.Drawing.Point(311, 425);
            this.lblSource.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(144, 30);
            this.lblSource.TabIndex = 56;
            this.lblSource.Text = "Source";
            // 
            // lblDestination
            // 
            this.lblDestination.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDestination.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold);
            this.lblDestination.Location = new System.Drawing.Point(311, 502);
            this.lblDestination.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDestination.Name = "lblDestination";
            this.lblDestination.Size = new System.Drawing.Size(144, 30);
            this.lblDestination.TabIndex = 57;
            this.lblDestination.Text = "Destination";
            // 
            // CreateDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(1235, 660);
            this.Controls.Add(this.lblDestination);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.lblSourceURL);
            this.Controls.Add(this.lblTargetURL);
            this.Controls.Add(this.txtRevNum);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.ckbxDraft);
            this.Controls.Add(this.ckbxActive);
            this.Controls.Add(this.lstbxDistributees);
            this.Controls.Add(this.lblCurrentDistributees);
            this.Controls.Add(this.btnSubmitNewDocument);
            this.Controls.Add(this.btnAddDistributee);
            this.Controls.Add(this.txtAddDistributee);
            this.Controls.Add(this.lblAddDistributee);
            this.Controls.Add(this.lblAttach);
            this.Controls.Add(this.btnCancelCreateNewDocument);
            this.Controls.Add(this.lblDocumentStatus);
            this.Controls.Add(this.lblCreateDocumentRecord);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtDocumentTitle);
            this.Controls.Add(this.lblRevisionNumber);
            this.Controls.Add(this.lblDocumentTitle);
            this.Controls.Add(this.btnBrowse);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CreateDocument";
            this.Text = "CreateDocument";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRevNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelCreateNewDocument;
        private System.Windows.Forms.Label lblDocumentStatus;
        private System.Windows.Forms.Label lblCreateDocumentRecord;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtDocumentTitle;
        private System.Windows.Forms.Label lblRevisionNumber;
        private System.Windows.Forms.Label lblDocumentTitle;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblAttach;
        private System.Windows.Forms.Label lblAddDistributee;
        private System.Windows.Forms.TextBox txtAddDistributee;
        private System.Windows.Forms.Button btnAddDistributee;
        private System.Windows.Forms.Button btnSubmitNewDocument;
        private System.Windows.Forms.Label lblCurrentDistributees;
        private System.Windows.Forms.ListBox lstbxDistributees;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox ckbxActive;
        private System.Windows.Forms.CheckBox ckbxDraft;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.NumericUpDown txtRevNum;
        private System.Windows.Forms.Label lblTargetURL;
        private System.Windows.Forms.Label lblSourceURL;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.Label lblDestination;
    }
}